#!/bin/bash
set -e

export this_file=$(readlink -f $0)

# waiting
t=$(nvidia-smi --query-gpu=memory.used --format=csv,noheader -i 0)
v=${t:0:-4}
echo "$(date '+%m-%d %H:%M') - ${t}/${v}"
while (( $v > 500 )); do
    sleep 300
    t=$(nvidia-smi --query-gpu=memory.used --format=csv,noheader -i 0)
    v=${t:0:-4}
    echo "$(date '+%m-%d %H:%M') - ${t}/${v}"
done

# COMET
export COMET_API_KEY=ZyQk1oWFZyaQvLVfrxJ6A4Add
export COMET_MODE=online
export COMET_MODEL_NAME=YOLOv8
export COMET_EVAL_BATCH_LOGGING_INTERVAL=1
export COMET_EVAL_LOG_CONFUSION_MATRIX=false
export COMET_EVAL_LOG_IMAGE_PREDICTIONS=false
export COMET_PROJECT_NAME=yolov8-coco8

# settings
yolo settings \
    weights_dir=/workspace/yolov8/references/assets \
    sync=False clearml=False comet=True dvc=False hub=False \
    mlflow=False neptune=False raytune=False tensorboard=False wandb=False

# train
data="data=/workspace/yolov8/references/assets/coco8.yaml"
model="model=/workspace/yolov8/references/assets/yolov8n.pt"
pretrained="pretrained=True"
epochs="epochs=10 patience=5"
warmup="warmup_epochs=1.0 warmup_momentum=0.8 warmup_bias_lr=0.1"
close_mosaic="close_mosaic=1"
batch="batch=64 nbs=64"
imgsz="imgsz=640"
save="save=True save_period=-1"
cache="cache=False"  # True/ram/disk/False
device="device=0,1,2,3,4,5,6,7"
project="project=/workspace/runs/yolov8"
name="name=$(basename ${this_file:0:-3})"  # "_t$(date +%m%d%H%M)"
# =SGD/Adam/Adamax/AdamW/NAdam/RAdam/RMSProp/auto
optimizer="optimizer=SGD verbose=True seed=0"
lr="lr0=0.001 lrf=0.1 momentum=0.937 weight_decay=0.0005"
augment="label_smoothing=0.0 scale=0.5 shear=0.0 perspective=0.0 mosaic=1.0 mixup=0.0 copy_paste=0.0 erasing=0.4"
# freeze: 10 - backbone, 22 - all
remaining="single_cls=False rect=False cos_lr=False freeze=None"
yolo detect train ${data} ${model} ${pretrained} ${epochs} ${warmup} \
    ${augment} ${close_mosaic} ${batch} ${imgsz} ${save} ${cache} ${device} \
    ${project} ${name} ${optimizer} ${lr} ${remaining}

cp ${data:5} ${project:8}/${name:5}
cp ${model:6} ${project:8}/${name:5}
cp ${this_file} ${project:8}/${name:5}
