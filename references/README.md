# yolov8

```sh
git config --global user.name "hejian.hh.198.docker-yolov8"
git config --global user.email "hejian.hh.198.docker-yolov8"

mkdir -p yolov8 && cd yolov8/
git clone -b main https://github.com/ultralytics/ultralytics.git .
git checkout --orphan latest
git add .
git commit -m "from 8.0.239, 2024/01/10"
git branch -M main

git remote rename origin ultralytics
git remote add origin git@gitlab.com:lab2204/yolov8.git
git push --set-upstream origin main

# 8.0.239 -> 8.1 :2024/01/11
git fetch ultralytics main:8.1
git merge 8.1 --allow-unrelated-histories
```

## Docker

```sh
n=docker-yolov8
t=flystarhe/yolov8:v8.1-torch2.1.2-cuda11.8
docker run --restart=always --gpus all -d -p 7002:9000 -p 7003:9001 --ipc=host --name ${n} --hostname ${n} -v "$(pwd)":/workspace ${t} ssh

docker exec -it ${TASK} bash
## tmux new -s ccc -d
## tmux a -t ccc
## train or .
```

**VS Code Remote Development**

```sh
Host gpu198-8x3090-docker-yolov8
    HostName 192.168.3.198
    Port 7002
    User root
    IdentityFile ~/.ssh/share_ssh/id_ed25519
```

## Train COCO8

```sh
# cd yolov8/
# pip install -e .

# cd yolov8/references/assets/
# unzip -q coco8.zip -d ../../datasets/

# 单GPU训练
yolo detect train data=coco8.yaml model=yolov8n.yaml pretrained=True epochs=100 imgsz=640
# 多GPU训练
yolo detect train data=coco8.yaml model=yolov8n.yaml pretrained=True epochs=100 imgsz=640 device=0,1
# 恢复训练
yolo train resume model=path/to/last.pt

# val
# - https://docs.ultralytics.com/modes/val/#arguments
yolo detect val model=path/to/best.pt
yolo detect val model=yolov8n.pt data=coco128.yaml batch=1 imgsz=640

# predict
# - https://docs.ultralytics.com/modes/predict/#inference-sources
# - https://docs.ultralytics.com/modes/predict/#inference-arguments
yolo detect predict model=path/to/best.pt source='https://ultralytics.com/images/bus.jpg'
yolo detect predict model=yolov8n.pt source='https://ultralytics.com/images/bus.jpg' imgsz=320

# export
# - https://docs.ultralytics.com/modes/export/#arguments
yolo export model=path/to/best.pt format=onnx
yolo export model=yolov8n.pt format=onnx imgsz=224,128
```
